package web;

import banelcolink.Clientes;
import banelcolink.DB1;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "consultaCliente", urlPatterns = {"/consultaCliente"})
public class consultaBanco extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String consultaSql = "SELECT * FROM `banco`";
            Connection conn = null;
            out.println("vamos a es banco");
            try {
                out.println("entramos en try");
                conn = DB1.getInstance().getConnection();
                PreparedStatement laCantidad = conn.prepareStatement(consultaSql);
                ResultSet losBancos = laCantidad.executeQuery();
                while (losBancos.next()) {

                    Banco miBanco1 = new Banco();
                    // miBanco1.id = losBancos.getString(ban_id);
                    miBanco1.bancos = losBancos.getString("ban_nom");
                    miBanco1.domicilio = losBancos.getString("ban_dom");
                    System.out.println(miBanco1);

                    out.println("<p>" + losBancos.getString("ban_id") + " | " + losBancos.getString("ban_nom") + " | " + losBancos.getString("ban_dom") + "</p>"
                    );

                }
            } catch (IOException | ClassNotFoundException | SQLException error) {
                out.println(error);
            } finally {
                if (conn != null) {
                    conn.close();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(consultaBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(consultaBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
