
package web;
import banelcolink.baseDeDatos;
import banelcolink.Clientes;
import  java.sql.Connection;
import banelcolink.DB1;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "ConsultaClientes", urlPatterns = {"/ConsultaClientes"})
public class ConsultaClientes extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
               out.println("<!DOCTYPE html>\n" +
"<html>\n" +
"<head>\n" +
"    <meta charset='utf-8'>\n" +
"    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n" +
"    <title>Listado de productos</title>\n" +
"    <meta name='viewport' content='width=device-width, initial-scale=1'>\n" +
"    <link rel='stylesheet' type='text/css' media='screen' href='css/stilos.css'>\n" +
"    <script src='main.js'></script>\n" +
"</head>\n" +
"<body>\n" +
"    <header>  <div id=\"logos\"></div>\n" +
"            <div id=\"logos1\"></div>\n" +
"            <div id=\"logotitulo\"></div>\n" +
"     </header>\n" +
"    <main>"
        + "<div class=\"wrap category-page\" ng-controller=\"Application.Controllers.Category.ProductList\">");
  
        String consultaSql = "SELECT * FROM `clientes`";
        Connection conn = null;
       // out.println("vamos a ver banco"  );
        try {
            //out.println("estamos dentro del try ");
            conn = DB1.getInstance().getConnection();
            PreparedStatement elSaludo = conn.prepareStatement(consultaSql);
           ResultSet resultado = elSaludo.executeQuery();
            while (resultado.next()) { 
                 Clientes miClientes1 = new Clientes();
      miClientes1.id = resultado.getString("cli_id");
      miClientes1.nombre = resultado.getString("cli_nom");
      miClientes1.clave = resultado.getString("cli_cla");
                System.out.println(miClientes1);
             //   out.println("<p>" + resultado.getString("cli_id") + " | " + resultado.getString("cli_nom") + " | " + resultado.getString("cli_cla") + "<p>");
             
              out.println("<div class=\"products\">\n" +
"        <div class=\"product-box\"\n" +
"             ng-repeat=\"product in products\">\n" +
"          <div class='title'>" + miClientes1.nombre        + "          " + " </div>\n" +
"          <div class='show-base'>\n" +
"            <img  width='180' src=\"" + miClientes1.clave + "\" alt=\"{{product.name}}\" />\n" +
"            <div class=\"mask\">\n" +
"              <h2>" +  miClientes1.nombre + "</h2>\n" +
"              <p ng-class=\"{old:product.specialPrice}\">" +  miClientes1.id + " </p>\n" +
"              <p ng-show=\"product.specialPrice\">"+miClientes1.id + "</p>\n" +
"              <div class=\"description\">\n" +
"                " + miClientes1.id + "\n" +
"              </div>\n" +
"              <div>\n" +
"                <a href=\"#\" class=\"more\">info</a>\n" +
"                <a href=\"#\" class=\"tocart\">Compra</a>\n" +
"              </div>\n" +
"            </div>\n" +
"          </div>\n" +
"        </div>\n" +
"      </div>");
            }
            
        } catch (IOException | ClassNotFoundException | SQLException error) {
            out.println(error);
        }finally{
            if (conn!= null) {
                conn.close();
            }
        }
     out.println("</main>\n" +
"    <footer></footer>\n" +
"</body>\n" +
"</html>");

        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ConsultaClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
